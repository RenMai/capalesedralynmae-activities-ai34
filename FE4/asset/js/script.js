/*let char1 = document.getElementById('barchart').getContext('2d');


let chart1 = new Chart(char1,{

    type:'bar',
    data: {
        labels:['Avengers','Harry Potter','GOT','Tom&Jerry,','Aquaman'],
        datasets:[{
            label: 'Most Watched Movie',
            data:['50','50','40','100','20'],
            backgroundColor:['blue','red','green','yellow','pink'],
        }]
    }

});

let ctx2 = document.getElementById('piechart').getContext('2d');

let chart2 = new Chart(ctx2,{

    type:'doughnut',
    data: {
        labels:['SciFi','Fantasy','Cartoon'],
        datasets:[{
            label: 'Popular Genre',
            data:['50','50','40','100','20'],
            backgroundColor:['blue','red']
        }]
    }

});*/

var chart1 = document.getElementById('canvas1').getContext('2d');
var chart2 = document.getElementById('canvas2').getContext('2d');

var chart = new Chart(chart1, {
    type:'bar',
    data: {
        labels:['Avengers','Harry Potter','GOT','Tom&Jerry,','Aquaman'],
        datasets:[{
            label: 'Most Watched Movie',
            data:['50','50','40','100','20'],
            backgroundColor:['#4cd137','#7f8fa6','#273c75','#353b48','p#c23616'],
        }]
    },

    
    options: {}
});

var chart = new Chart(chart2, {
    
    type:'doughnut',
    data: {
        labels:['SciFi','Fantasy','Cartoon'],
        datasets:[{
            label: 'Popular Genre',
            data:['50','50','100'],
            backgroundColor:['#1e3799','#b71540','#079992']
        }]
    },

    
    options: {}
});